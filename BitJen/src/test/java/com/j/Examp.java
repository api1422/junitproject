package com.j;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

import com.bdemo.Palindrome;
import com.bdemo.RevereseNum;

public class Examp {
	
	static Palindrome obj;
	static RevereseNum obj2;
	@BeforeAll
	public static void avc1()
	{
		obj=new Palindrome();
		obj2=new RevereseNum();
		System.out.println("I am BeforeAll-");
	}
	@BeforeEach
	public  void avc2()
	{
		System.out.println("I am BeforeEach..");
		
	}
	@Test
	public void avc3()
	{
		assertEquals(obj.isPalindrome(67),false);
		System.out.println("I am TestCAse");
		
	}
	@AfterAll
	public static void avc4()
	{
		System.out.println("I am BeforeEach");
		
	}
	@AfterAll
	public static void avc5()
	{
		System.out.println("I am AfterAll");
		
	}
	@Test
	public void avc6()
	{
		assertEquals(obj.isPalindrome(121),true);
		System.out.println("I am TestCAse");
		
	}
	@Test
	public void avc8()
	{
		assertEquals(obj2.revers(16),61);
		System.out.println("I am TestCAse");
		
	}
	@Test
	public void avc9()
	{
		assertEquals(obj2.revers(89),98);
		System.out.println("I am TestCAse");
		
	}

}
